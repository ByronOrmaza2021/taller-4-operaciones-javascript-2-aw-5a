/* Conversion de Hora Actual a Segundo*/
function Hora_Segundo(){
    let d = new Date();
    const H = ((d.getHours() < 10) ? "0" : "") + d.getHours();
    const M = ((d.getMinutes() < 10) ? "0" : "") + d.getMinutes();
    const S = ((d.getSeconds() < 10) ? "0" : "") + d.getSeconds();

    var C1 = H * 3600;
    var C2 = M * 60;    
    var C3 = S * 1;

    var seconds = C1 + C2 + C3
    
    alert("Conversion - Hora actual a Segundos")
    alert("Hora actual: " + H + ":" + M + ":" + S)
    alert("Hora actual en segundos: " + seconds)
}

/* Calcular area de un triangulo */
function calcular_area(){
    var base = document.getElementById("Base_T").value;
    var altura = document.getElementById("Altura_T").value;
    var area = (parseFloat(base) * parseFloat(altura))/2
    alert("El resultado del area del triangulo es: " + area)
}

/* Raiz cuadrada impar */
function calcular_raiz_cuadrada(){
    var numero = document.getElementById("Impar_N").value;
    if(numero % 2 == 0){
        alert("Error ... el número que ingreso es PAR.")
    }
    else{
        var result = Math.sqrt(numero)
        var Resultado = result.toFixed(2)
        alert("Resultado de la raíz cuadrada: " + Resultado)
    }
}


/* Cadena Longitud */
function calcular_longitud(){
    var texto = document.getElementById("Cadena_L").value;
    alert("Texto ingresado: " + texto)
    alert("Longitud del texto ingresado: " + texto.length)
}

function mostrar_navegador(){
    alert("Nombre del Navegador: "+ navigator.appName); 
    alert("Version del Navegador: " + navigator.appVersion);
}


/* Tamaño Pantalla */
function MostrarAlturaAncho() {
    var Alto = screen.height;
    var Ancho = screen.width;
    alert("La pantalla tiene un tamaño de: " + Ancho +" de ancho y " + Alto +" de altura.");
}


/* Imprimir Página */
function ImprimirPagina(){
    window.print();
}